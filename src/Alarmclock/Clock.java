package Alarmclock;

public class Clock {

    public Clock () {

    }

    public long secToms(int sec){
        return sec * 1000;
    }

    public long minToms(int min){
        return secToms(60 * min);
    }

    public long hourToms(int hour){
        return minToms(60*hour);
    }

    public long getCurrentTime(){
        return System.currentTimeMillis();
    }

    public boolean isTimedOut(long time){
        if(time < getCurrentTime()){
            return true;
        }
        else{
            return false;
        }
    }
}
