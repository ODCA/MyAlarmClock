package Alarmclock;

import java.util.HashMap;
import java.util.Map;

public class Alarmhandler {

    Map<String, Alarm> alarms;
    public Alarmhandler(){
        alarms = new HashMap<>();
    }

    public void addAlarm(String name, Alarm alarm){
        alarms.put(name, alarm);
    }

    public void stopSound(){
        for(Map.Entry<String,Alarm> entry : alarms.entrySet()){
            entry.getValue().stopSound();
        }
    }

    public Alarm getAlarm(String name){
        return alarms.get(name);
    }

    public void removeAlarm(String name){
        alarms.remove(name);
    }



}
