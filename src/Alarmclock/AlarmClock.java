package Alarmclock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class AlarmClock {

    private Clock clock;
    private Alarmhandler alarmhandler;

    public AlarmClock() throws IOException{
        clock = new Clock();
        alarmhandler = new Alarmhandler();
    }

    //call when user want new Timer to be set
    public void createNewAlarm(String name) {

        System.out.println("Enter time until Alarm sound \n" +
                "Enter time on following type: --:--:-- (hh:mm:ss)");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String time = " ";
        boolean correctInput = false;
        int[] inNbr = {0,0,0};

        while (!correctInput){
            try {


                time = reader.readLine();
                String[] separated = time.split(":");
                inNbr = new int[separated.length];
                for (int i = 0; i < separated.length; i++) {
                    inNbr[i] = Integer.parseInt(separated[i]);
                    correctInput = true;
                }
            }catch (IOException | NumberFormatException e){
                System.out.println("Wrong input format, \nCorrect format is -:-:- (h:m:s) ex: 0:2:45 = 2 min 45 sec");
                correctInput = false;
            }
        }

        long timems = clock.hourToms(inNbr[0]) + clock.minToms(inNbr[1]) + clock.secToms(inNbr[2]);

        alarmhandler.addAlarm(name, new Alarm(name, timems, clock));
    }

    public void startAlarm(String name){
        alarmhandler.getAlarm(name).start();
    }

    public void pauseAlarm(String name){
        alarmhandler.getAlarm(name).paus();
    }

    public void removeAlarm(String name){
        alarmhandler.removeAlarm(name);
    }

    public void stopSound(){
        alarmhandler.stopSound();
    }

}
