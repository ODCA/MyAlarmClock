package Alarmclock;

import java.applet.Applet;
import java.applet.AudioClip;

public class Alarm {

    private String name;
    private long time; // time left on timer
    private long timedOutTime; //time when alarm will call
    private Clock clock;
    private AudioClip clip;
    private Boolean playing = false;

    public Alarm (String name, long time, Clock clock) {
        this.name = name;
        this.time = time;
        this.clock = clock;
    }

    public void start () {
        timedOutTime = time + clock.getCurrentTime();
        System.out.println("Timer Started");

        new Thread(() -> {
            while (!clock.isTimedOut(timedOutTime)) {
                //clock running for x ms
            }
            callAlarm();
        }).start();

    }

    public void paus () {
        System.out.println("Timer paused");
    }

    private void callAlarm () {
        System.out.println("*************--ALARM--***********\n>>");

        clip = Applet.newAudioClip(getClass().getResource("/Alarm.wav"));
        clip.loop();
        playing = true;
    }

    public void stopSound(){
        if(playing){
            clip.stop();
            playing = false;
        }
    }

}
