package UI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TimeApplication {

    private InputHandler inputHandler;


    public static void main (String[] args) {
        try {
            new TimeApplication();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TimeApplication () throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        inputHandler = new InputHandler();
        System.out.println("Welcome to Alarmclock\n" +
                "Write comands below: 'h' or 'help' for command list");
        while (true){
            System.out.print(">>");
            String input = reader.readLine();
            System.out.println(input);
            inputHandler.userInput(input);

        }
    }
}
