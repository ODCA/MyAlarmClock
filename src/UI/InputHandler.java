package UI;

import Alarmclock.AlarmClock;


import java.io.IOException;

public class InputHandler {
    private AlarmClock alarmClock;

    public InputHandler() {
        try {
            alarmClock = new AlarmClock();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }{}

    public void userInput(String input){
        input.toLowerCase();
        String[] command = input.split(" ");
        String name = "";

        alarmClock.stopSound();

        if(command.length > 1){
            name = command[1];
        }

        if(command[0].equals("h") || command[0].equals("help")){
            printHelp();
        }

        else if(command[0].equals("new alarm") || command[0].equals("new")){
            alarmClock.createNewAlarm(name);
            System.out.println("new Alarm Created");
        }

        else if (command[0].equals("start") || command[0].equals("s")){
            alarmClock.startAlarm(name);
            System.out.println(name + " Started");
        }

        else if(command[0].equals("paus") || command[0].equals("p")){
            alarmClock.pauseAlarm(name);
            System.out.println(name + " paused");
        }

        else if(command[0].equals("remove") || command[0].equals("r")){
            alarmClock.removeAlarm(name);
            System.out.println(name + " removed");
        }


        else {
            System.out.println("Command not recognised");
        }
    }

    private void printHelp(){
        System.out.println("'h', 'help' - gives you help \n" +
                "");
    }
}
